/*global -$ */
'use strict';

// generated on 2015-04-01 using generator-gulp-webapp 0.3.0
var gulp = require('gulp');
var inlineCss = require('gulp-inline-css');

var paths = {
	src: 'in',
	dest: 'out'
};

// connect
gulp.task('inlinecss', function() {
	return gulp.src(paths.src + '/*.html')
		.pipe(inlineCss())
		.pipe(gulp.dest(paths.dest));
});
